import 'package:prof2024/data/models/model_auth.dart';
import 'package:prof2024/data/repository/supabase.dart';
import 'package:prof2024/presentation/utlis.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class SignUpPresenter{

  Future<void> pressSignUp(
      String email,
      String password,
      Function(AuthResponse) onResponse,
      Function(String) onError
      ) async {
      try{
        ModelAuth modelAuth = ModelAuth(email: email, password: password);
        dynamic result = await signUp(modelAuth);
        onResponse(result);
      } on PostgrestException catch (e){
        onError(e.message);
      } on Exception catch (e){
        onError(e.toString());
      }
  }
}