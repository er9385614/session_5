import 'package:prof2024/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class HolderPresenter{

  Future<void> pressLogOut(
      Function(void) onResponse,
      Function(String) onError
      ) async {
    try{
      var result = await logOut();
      onResponse(result);
    } on AuthException catch (e){
      onError(e.message);
    } on PostgrestException catch (e){
      onError(e.message);
    } on Exception catch (e){
      onError(e.toString());
    }
  }
}