import 'package:flutter/material.dart';
import 'package:prof2024/common/colors.dart';
import 'package:prof2024/common/theme.dart';
import '../presentation/pages/sign_up_page.dart';

class MyApp extends StatefulWidget {

  bool isLightTheme = true;

  MyApp({super.key});

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  void changeDifferentTheme(BuildContext context){
    isLightTheme = !isLightTheme;
    context.findAncestorStateOfType<_MyAppState>()?.changeTheme();
  }

  ColorsApp getColorsApp(BuildContext context){
    return (isLightTheme) ? light : dark;
  }

  ThemeData getCurrentTheme(){
    return (isLightTheme) ? lightTheme : darkTheme;
  }
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  void changeTheme(){
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: widget.getCurrentTheme(),
      home: SignUpPage(),
    );
  }
}