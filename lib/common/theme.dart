import 'package:flutter/material.dart';
import 'package:prof2024/common/colors.dart';

var light = LightColorsApp();

var lightTheme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      color: light.text,
      fontSize: 24,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w500
    ),
    titleMedium: TextStyle(
        color: light.subText,
        fontSize: 14,
        fontFamily: "Roboto",
        fontWeight: FontWeight.w500
    ),
    titleSmall: TextStyle(
        color: light.hint,
        fontSize: 14,
        fontFamily: "Roboto",
        fontWeight: FontWeight.w500
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: light.accent,
      textStyle: TextStyle(
        color: light.disableTextAccent,
        fontWeight: FontWeight.w700,
        fontFamily: "Roboto",
        fontSize: 16
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4),
      ),
      padding: EdgeInsets.symmetric(horizontal: 36, vertical: 15)
    )
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
        textStyle: TextStyle(
            color: light.disableTextAccent,
            fontWeight: FontWeight.w700,
            fontFamily: "Roboto",
            fontSize: 16
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
        ),
        padding: EdgeInsets.symmetric(horizontal: 36, vertical: 15)
    )
  )
);

var dark = DarkColorsApp();

var darkTheme = ThemeData(
    textTheme: TextTheme(
      titleLarge: TextStyle(
          color: dark.text,
          fontSize: 24,
          fontFamily: "Roboto",
          fontWeight: FontWeight.w500
      ),
      titleMedium: TextStyle(
          color: dark.subText,
          fontSize: 14,
          fontFamily: "Roboto",
          fontWeight: FontWeight.w500
      ),
      titleSmall: TextStyle(
          color: dark.hint,
          fontSize: 14,
          fontFamily: "Roboto",
          fontWeight: FontWeight.w500
      ),
    ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            backgroundColor: dark.accent,
            textStyle: TextStyle(
                color: dark.disableTextAccent,
                fontWeight: FontWeight.w700,
                fontFamily: "Roboto",
                fontSize: 16
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            padding: EdgeInsets.symmetric(horizontal: 36, vertical: 15)
        )
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
            textStyle: TextStyle(
                color: dark.disableTextAccent,
                fontWeight: FontWeight.w700,
                fontFamily: "Roboto",
                fontSize: 16
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            padding: EdgeInsets.symmetric(horizontal: 36, vertical: 15)
        )
    )
);