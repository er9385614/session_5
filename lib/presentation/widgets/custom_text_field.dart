import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:prof2024/common/app.dart';

class CustomTextField extends StatefulWidget{

  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;

  const CustomTextField(
      {
        super.key,
        required this.label,
        required this.hint,
        required this.controller,
        this.enableObscure = false});
  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool isObscure = true;

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 24,),
        Text(widget.label, style: Theme.of(context).textTheme.titleMedium,),
        SizedBox(height: 8,),
        SizedBox(
          height: 44,
          width: double.infinity,
          child: TextField(
            controller: widget.controller,
            obscuringCharacter: "*",
            obscureText: (widget.enableObscure) ? isObscure : false,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
              hintText: widget.hint,
              hintStyle: Theme.of(context).textTheme.titleSmall,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4),
                  borderSide: BorderSide(
                      color: colors.subText,
                      width: 1
                  )
              ),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4),
                  borderSide: BorderSide(
                      color: colors.subText,
                      width: 1
                  )
              ),
              suffixIcon: (widget.enableObscure) ?
              GestureDetector(
                onTap: (){
                  setState(() {
                    isObscure = !isObscure;
                  });
                },
                child: Image.asset("assets/eye-slash.png")
              ) : null
            ),
          ),
        )
      ],
    );
  }
}