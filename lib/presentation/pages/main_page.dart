import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:prof2024/common/app.dart';
import 'package:prof2024/presentation/pages/tabs/favorite_tab.dart';
import 'package:prof2024/presentation/pages/tabs/home_tab.dart';
import 'package:prof2024/presentation/pages/tabs/profile_tab.dart';
import 'package:prof2024/presentation/pages/tabs/search_tab.dart';

class MainPage extends StatefulWidget{

  final int initialIndex;

  const MainPage(
      {
        super.key,
        this.initialIndex = 0
      }
    );
  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  @override
  void initState(){
    super.initState();
    setState(() {

    });
    currentIndex = widget.initialIndex;
  }


  var currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      bottomNavigationBar: BottomNavigationBar(
        elevation: 0,
        type: BottomNavigationBarType.fixed,
        unselectedFontSize: 12,
        selectedFontSize: 12,
        selectedItemColor: colors.accent,
        unselectedItemColor: colors.subText,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        onTap: (newValue){
          setState(() {
            currentIndex = newValue;
          });
        },
        currentIndex: currentIndex,
        backgroundColor: colors.background,
        items: [
          BottomNavigationBarItem(
            label: "Home",
            icon: (currentIndex == 0)?
                SvgPicture.asset("assets/home_selected.svg",
                color: colors.accent,) :
                SvgPicture.asset("assets/home.svg",
                color: colors.subText)
          ),
          BottomNavigationBarItem(
              label: "Search",
              icon: (currentIndex == 1)?
              SvgPicture.asset("assets/search_selected.svg",
                color: colors.accent,) :
              SvgPicture.asset("assets/search.svg",
                  color: colors.subText)
          ),
          BottomNavigationBarItem(
              label: "Favorite",
              icon: (currentIndex == 2)?
              SvgPicture.asset("assets/favorite_selected.svg",
                color: colors.accent,) :
              SvgPicture.asset("assets/favorite.svg",
                  color: colors.subText)
          ),
          BottomNavigationBarItem(
              label: "Profile",
              icon: (currentIndex == 3)?
              SvgPicture.asset("assets/profile_selected.svg",
                color: colors.accent,) :
              SvgPicture.asset("assets/profile.svg",
                  color: colors.subText)
          )
        ],
      ),
      body: [
        HomeTab(), SearchTab(), FavoriteTab(), ProfileTab()
      ][currentIndex],
    );
  }
}