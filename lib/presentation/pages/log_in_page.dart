import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:prof2024/common/colors.dart';
import 'package:prof2024/domain/log_in_presenter.dart';
import 'package:prof2024/presentation/pages/forgot_password_page.dart';
import 'package:prof2024/presentation/pages/holder_page.dart';
import 'package:prof2024/presentation/pages/main_page.dart';
import 'package:prof2024/presentation/pages/sign_up_page.dart';
import 'package:prof2024/presentation/widgets/custom_text_field.dart';

import '../utlis.dart';

class LogInPage extends StatefulWidget{
  @override
  State<LogInPage> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {

  bool isRememberMe = false;
  var email = TextEditingController();
  var password = TextEditingController();

  var presenter = LogInPresenter();

  bool isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.background,
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 83),
            Text("Добро пожаловать",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Заполните почту и пароль чтобы продолжить",
              style: Theme.of(context).textTheme.titleMedium,),
            SizedBox(height: 4,),
            CustomTextField(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email),
            CustomTextField(
              label: "Пароль",
              hint: "**********",
              controller: password,
              enableObscure: isObscurePassword,),
            SizedBox(height: 18,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox.square(
                      dimension: 22,
                      child: Transform.scale(
                        scale: 1.2,
                        child: Checkbox(
                          side: BorderSide(
                              width: 1,
                              color: colors.subText
                          ),
                          activeColor: colors.accent,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),

                          ),
                            value: isRememberMe,
                            onChanged: (newValue){
                              setState(() {
                                isRememberMe = !isRememberMe;
                              });
                            }),
                      ),
                    ),
                    SizedBox(width: 8,),
                    Text("Запомнить меня",
                      style: Theme.of(context).textTheme.titleMedium,)
                  ],
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (_) => ForgotPasswordPage()
                    ));
                  },
                  child: Text("Забыли пароль?",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      color: colors.accent
                    )),
                )
              ],
            ),
            Expanded(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: 46,
                        width: double.infinity,
                        child: FilledButton(
                            onPressed: (){
                              presenter.pressLogIn(
                                  email.text,
                                  password.text,
                                      (_) => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (_) => const MainPage(initialIndex: 0,)
                                      )
                                  ), (error) => showErrorDialog(context, error));
                            },
                            child: Text("Войти")),
                      ),
                      SizedBox(height: 14,),
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (_) => SignUpPage()));
                        },
                        child: RichText(text: TextSpan(
                            children: [
                              TextSpan(text: "У меня нет аккаунта! ",
                                  style: Theme.of(context).textTheme.titleMedium),
                              TextSpan(text: "Создать",
                                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                      color: colors.accent
                                  ))
                            ]
                        )),
                      ),
                      SizedBox(height: 32,)
                    ]
                ))
          ],
        ),
      ),
    );
  }
}
