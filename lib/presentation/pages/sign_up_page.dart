import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:prof2024/common/colors.dart';
import 'package:prof2024/domain/sign_up_presenter.dart';
import 'package:prof2024/presentation/pages/holder_page.dart';
import 'package:prof2024/presentation/pages/log_in_page.dart';
import 'package:prof2024/presentation/pages/main_page.dart';
import 'package:prof2024/presentation/widgets/custom_text_field.dart';

import '../utlis.dart';

class SignUpPage extends StatefulWidget{
  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  var email = TextEditingController();
  var password = TextEditingController();
  var confirmPassword = TextEditingController();

  var presenter = SignUpPresenter();

  bool isObscurePassword = true;
  bool isObscureConfirmPassword = true;

  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.background,
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 83),
            Text("Создать аккаунт",
            style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Завершите регистрацию чтобы начать",
            style: Theme.of(context).textTheme.titleMedium,),
            SizedBox(height: 4,),
            CustomTextField(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email),
            CustomTextField(
                label: "Пароль",
                hint: "**********",
                controller: password,
                enableObscure: isObscurePassword,),
            CustomTextField(
                label: "Повторите пароль",
                hint: "**********",
                controller: confirmPassword,
                enableObscure: isObscureConfirmPassword,),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: 46,
                      width: double.infinity,
                      child: FilledButton(
                          onPressed: (){
                            presenter.pressSignUp(
                                email.text,
                                password.text,
                                    (_) => Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (_) => const MainPage(initialIndex: 0)
                                        )
                                    ), (error) => showErrorDialog(context, error));
                          },
                          child: Text("Зарегистрироваться")),
                    ),
                    SizedBox(height: 14,),
                    GestureDetector(
                      onTap: (){
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (_) => LogInPage()
                        ), (route) => false);
                      },
                      child: RichText(text: TextSpan(
                        children: [
                          TextSpan(text: "У меня уже есть аккаунт! ",
                              style: Theme.of(context).textTheme.titleMedium),
                          TextSpan(text: "Войти",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                color: colors.accent
                              ))
                        ]
                      )),
                    ),
                    SizedBox(height: 32,)
                  ]
                ))
          ],
        ),
      ),
    );
  }
}
