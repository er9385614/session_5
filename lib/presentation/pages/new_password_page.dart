import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:prof2024/common/colors.dart';
import 'package:prof2024/domain/new_pasword_presenter.dart';
import 'package:prof2024/presentation/pages/log_in_page.dart';
import 'package:prof2024/presentation/widgets/custom_text_field.dart';

import '../utlis.dart';

class NewPasswordPage extends StatefulWidget{
  @override
  State<NewPasswordPage> createState() => _NewPasswordPageState();
}

class _NewPasswordPageState extends State<NewPasswordPage> {

  var password = TextEditingController();
  var confirmPassword = TextEditingController();

  var presenter = NewPasswordPresenter();

  bool isObscurePassword = true;
  bool isObscureConfirmPassword = true;

  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.background,
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 83),
            Text("Новый пароль",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Введите новый пароль",
              style: Theme.of(context).textTheme.titleMedium,),
            SizedBox(height: 4,),
            CustomTextField(
              label: "Пароль",
              hint: "**********",
              controller: password,
              enableObscure: isObscurePassword,),
            CustomTextField(
              label: "Повторите пароль",
              hint: "**********",
              controller: confirmPassword,
              enableObscure: isObscureConfirmPassword,),
            Expanded(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: 46,
                        width: double.infinity,
                        child: FilledButton(
                            onPressed: (){
                              presenter.pressUpdatePassword(
                                  password.text,
                                      (_) => Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (_) => LogInPage())),
                                      (error) => showErrorDialog(context, error));
                            },
                            child: Text("Подтвердить")),
                      ),
                      SizedBox(height: 14,),
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (_) => LogInPage()));
                        },
                        child: RichText(text: TextSpan(
                            children: [
                              TextSpan(text: "Я вспомнил свой пароль! ",
                                  style: Theme.of(context).textTheme.titleMedium),
                              TextSpan(text: "Вернуться",
                                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                      color: colors.accent
                                  ))
                            ]
                        )),
                      ),
                      SizedBox(height: 32,)
                    ]
                ))
          ],
        ),
      ),
    );
  }
}
