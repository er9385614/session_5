import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:prof2024/common/app.dart';

class ChoosePlatformPage extends StatefulWidget{
  @override
  State<ChoosePlatformPage> createState() => _ChoosePlatformPageState();
}

class _ChoosePlatformPageState extends State<ChoosePlatformPage> {
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 22),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 67),
            Row(
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pop();
                  },
                  child: SvgPicture.asset("assets/remove.svg",
                  color: colors.iconTint,),
                ),
                SizedBox(width: 16,),
                Text("Выберите платформу",
                style: Theme.of(context).textTheme.titleLarge?.copyWith(
                  fontSize: 22
                ),)
              ],
            )
            ],
        ),
      ),
    );
  }
}