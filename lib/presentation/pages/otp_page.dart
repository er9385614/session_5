import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pinput/pinput.dart';
import 'package:prof2024/common/colors.dart';
import 'package:prof2024/domain/otp_presenter.dart';
import 'package:prof2024/presentation/pages/new_password_page.dart';
import 'package:prof2024/presentation/pages/sign_up_page.dart';

import '../utlis.dart';


class OTPVerificationPage extends StatefulWidget{

  final String email;

  const OTPVerificationPage({super.key, required this.email});
  @override
  State<OTPVerificationPage> createState() => _OTPVerificationPageState();
}

class _OTPVerificationPageState extends State<OTPVerificationPage> {

  var code = TextEditingController();

  var presenter = OTpVerificationPresenter();


  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.background,
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 83),
            Text("Верификация",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Введите 6-ти значный код из письма",
              style: Theme.of(context).textTheme.titleMedium,),
            SizedBox(height: 58,),
            Pinput(
              length: 6,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              controller: code,
              defaultPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1),
                  border: Border.all(
                    color: colors.subText
                  )
                )
              ),
            ),
            Expanded(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: 46,
                        width: double.infinity,
                        child: FilledButton(
                            onPressed: (){
                              presenter.pressSendOTP(
                                  widget.email,
                                  code.text,
                                      (_) => Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (_) => NewPasswordPage())),
                                      (error) => showErrorDialog(context, error));

                            },
                            child: Text("Сбросить пароль")),
                      ),
                      SizedBox(height: 14,),
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (_) => SignUpPage()));
                        },
                        child: RichText(text: TextSpan(
                            children: [
                              TextSpan(text: "Я вспомнил свой пароль! ",
                                  style: Theme.of(context).textTheme.titleMedium),
                              TextSpan(text: "Вернуться",
                                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                      color: colors.accent
                                  ))
                            ]
                        )),
                      ),
                      SizedBox(height: 32,)
                    ]
                ))
          ],
        ),
      ),
    );
  }
}
