import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:prof2024/common/app.dart';
import 'package:prof2024/presentation/pages/choose_platform_page.dart';
import 'package:prof2024/presentation/pages/main_page.dart';
import 'package:prof2024/presentation/widgets/custom_text_field.dart';

class SetupProfilePage extends StatefulWidget{
  @override
  State<SetupProfilePage> createState() => _SetupProfilePageState();


}

class _SetupProfilePageState extends State<SetupProfilePage> {
  var fio = TextEditingController();
  var phone = MaskedTextController(mask: "+7 (000) 000 00 00");
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment:  CrossAxisAlignment.start,
            children: [
              SizedBox(height: 73,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: colors.accent
                      ),
                      borderRadius: BorderRadius.circular(14)
                    ),
                    width: 45,
                    height: 45,
                    child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(14),
                          side: BorderSide(
                            color: colors.accent
                          )
                        ),
                        minimumSize: Size.zero,
                        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12)
                      ),
                        onPressed: (){},
                        child: Icon(Icons.add_photo_alternate_outlined,
                        color: colors.accent, size: 21,)),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(32),
                      color: colors.block
                    ),
                    width: 151,
                    height: 151,
                    child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 32, vertical: 32),
                        child: SvgPicture.asset("assets/avatar_1.svg")),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: colors.accent
                        ),
                        borderRadius: BorderRadius.circular(14)
                    ),
                    width: 45,
                    height: 45,
                    child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(14),
                                side: BorderSide(
                                    color: colors.accent
                                )
                            ),
                            minimumSize: Size.zero,
                            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12)
                        ),
                        onPressed: (){
                          setState(() {
                            MyApp.of(context).changeDifferentTheme(context);
                          });
                        },
                        child: Icon(Icons.sunny,
                          color: colors.accent, size: 21,)),
                  ),
                ],
              ),
              CustomTextField(
                  label: "ФИО",
                  hint: "Введите ваше ФИО",
                  controller: fio),
              CustomTextField(
                  label: "Телефон",
                  hint: "+7 (000) 000 00 00",
                  controller: phone),
              SizedBox(height: 24,),
              Text("Выберите дату",
                style: Theme.of(context).textTheme.titleMedium),
              SizedBox(height: 8,),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: colors.subText,
                  ),
                  borderRadius: BorderRadius.circular(4)
                ),
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 11),
                width: double.infinity,
                child: Text("Выберите дату",
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      color: colors.text
                  ),),
              ),
              SizedBox(height: 24,),
              Text("Источники новостей",
                  style: Theme.of(context).textTheme.titleMedium),
              SizedBox(height: 8,),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(
                      color: colors.subText,
                    ),
                    borderRadius: BorderRadius.circular(4)
                ),
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 11),
                width: double.infinity,
                child: Column(
                  children: [
                    Row(
                        children: [
                          Expanded(
                            child: Text("Выберите платформы",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                  color: colors.text
                              ),),
                          ),
                          GestureDetector(
                            onTap: (){
                              Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (_) => ChoosePlatformPage()));
                            },
                            child: SvgPicture.asset("assets/add.svg",
                            color: colors.iconTint,),
                          )
                        ]
                    ),
                  ],
                )
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      height: 46,
                      width: double.infinity,
                      child: FilledButton(
                          onPressed: (){
                            Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (_) => MainPage(initialIndex: 3,)
                                ));
                          },
                          child: Text("Сохранить")),
                    ),
                    SizedBox(height: 18,),
                    SizedBox(
                      height: 46,
                      width: double.infinity,
                      child: OutlinedButton(
                          style: OutlinedButton.styleFrom(
                              side: BorderSide(
                                  color: colors.accent
                              )
                          ),
                          onPressed: (){
                            Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (_) => MainPage(initialIndex: 3,)
                            ));
                          },
                          child: Text("Отменить",
                            style: TextStyle(
                                color: colors.accent,
                                fontSize: 16,
                                fontWeight: FontWeight.w700
                            ),)),
                    ),
                    SizedBox(height: 32,)
                  ],
                ),
              )
            ],
          )),
    );
  }
}