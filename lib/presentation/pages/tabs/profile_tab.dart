import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:prof2024/common/app.dart';
import 'package:prof2024/presentation/pages/setup_profile_page.dart';

class ProfileTab extends StatefulWidget{
  @override
  State<ProfileTab> createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 73,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(32),
                      color: colors.block
                  ),
                  width: 121,
                  height: 121,
                  child: Image.asset("assets/avatar.png"),
                ),
                SizedBox(width: 18,),
                Expanded(
                    child: Text("Смирнов Александр Максимович",
                    style: TextStyle(
                      color: colors.text,
                      fontSize: 18,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.w500
                    ),))
              ],
            ),
            SizedBox(height: 28,),
            Divider(height: 1,
            color: colors.subText,),
            SizedBox(height: 25,),
            Text("История",
            style: TextStyle(
              color: colors.text,
              fontWeight: FontWeight.w600,
              fontFamily: "Roboto",
              fontSize: 18
            ),),
            SizedBox(height: 18,),
            Text("Прочитанные статьи",
              style: TextStyle(
                  color: colors.text,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto",
                  fontSize: 14
              ),),
            SizedBox(height: 18,),
            Text("Чёрный список",
              style: TextStyle(
                  color: colors.text,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto",
                  fontSize: 14
              ),),
            SizedBox(height: 24,),
            Text("Настройки",
              style: TextStyle(
                  color: colors.text,
                  fontWeight: FontWeight.w600,
                  fontFamily: "Roboto",
                  fontSize: 18
              ),),
            SizedBox(height: 18,),
            GestureDetector(
              onTap: (){
                Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (_) => SetupProfilePage()
                ));
              },
              child: Text("Редактирование профиля",
                style: TextStyle(
                    color: colors.text,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto",
                    fontSize: 14
                ),),
            ),
            SizedBox(height: 18,),
            Text("Политика конфиденциальности",
              style: TextStyle(
                  color: colors.text,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto",
                  fontSize: 14
              ),),
            SizedBox(height: 18,),
            Text("Оффлайн чтение",
              style: TextStyle(
                  color: colors.text,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto",
                  fontSize: 14
              ),),
            SizedBox(height: 18,),
            Text("О нас",
              style: TextStyle(
                  color: colors.text,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto",
                  fontSize: 14
              ),),
            SizedBox(height: 24,),
            SizedBox(
              height: 46,
              width: double.infinity,
              child: OutlinedButton(
                style: OutlinedButton.styleFrom(
                  side: BorderSide(
                    color: colors.error
                  )
                ),
                  onPressed: (){},
                  child: Text("Выход",
                  style: TextStyle(
                      color: colors.error,
                      fontSize: 16,
                      fontWeight: FontWeight.w400
                  ),)),
            )
          ],
        ),
      ),
    );
  }
}