import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:prof2024/common/app.dart';
import 'package:prof2024/data/models/model_news.dart';
import 'package:prof2024/data/models/model_platform.dart';
import 'package:prof2024/data/repository/supabase.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class SearchTab extends StatefulWidget {
  @override
  State<SearchTab> createState() => _SearchTabState();
}

class _SearchTabState extends State<SearchTab> {
  var currentIndex = 0;

  @override
  void initState() {
    super.initState();
    getAllPlatforms().then((value) {
      setState(() {
        allPlatforms = value;
      });
      getNews().then((value) {
        setState(() {
          newsList = value.reversed.toList().where(
                  (element) => element.geoLat != null).toList();
        });
      });
    });
  }

  List<PlacemarkMapObject> yandexMap() {
    if (newsList.isEmpty) {
      return [];
    }
    var currentModelNews = newsList[currentIndex];
      return newsList
          .map((e) => PlacemarkMapObject(
                mapId: MapObjectId("marker-${e.id}"),
                opacity: 1,
                icon: PlacemarkIcon.single(PlacemarkIconStyle(
                    image:
                        BitmapDescriptor.fromAssetImage("assets/Ellipse 2.png"),
                    scale: 8)),
                point: Point(latitude: e.geoLat!, longitude: e.geoLong!),
              ))
          .toList();
  }

  List<ModelPlatform> allPlatforms = [];

  Widget getNewItem(ModelNews news) {
    var colors = MyApp.of(context).getColorsApp(context);
    var platforms = news.getModelPlatform(allPlatforms);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 12,
        ),
        Row(
          children: [
            Image.network(
              platforms.icon,
              width: 30,
              height: 30,
            ),
            SizedBox(
              width: 12,
            ),
            Column(children: [
              RichText(
                  text: TextSpan(children: [
                TextSpan(
                    text: platforms.channels,
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium
                        ?.copyWith(fontSize: 16)),
                TextSpan(
                    text: platforms.title,
                    style: TextStyle(
                        color: colors.text,
                        fontSize: 12,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.w400))
              ])),
              Text(news.dateFormat())
            ])
          ],
        ),
        Text(news.title),
        Text(news.getText())
      ],
    );
  }

  List<ModelNews> newsList = [];

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Column(
        children: [
          Expanded(
              child: YandexMap(
                mapObjects: yandexMap(),
              )),
          SizedBox(
            height: 336,
            child: PageView.builder(
                itemCount: newsList.length,
                itemBuilder: (context, index) {
                  return getNewItem(newsList[index]);
                }),
          )
        ],
      ),
    );
  }
}
