import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prof2024/common/colors.dart';
import 'package:prof2024/domain/holder_presenter.dart';

import '../utlis.dart';

class HolderPage extends StatefulWidget{
  @override
  State<HolderPage> createState() => _HolderPageState();
}

class _HolderPageState extends State<HolderPage> {
  var presenter = HolderPresenter();
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Center(
          child:
          SizedBox(
            height: 46,
            width: double.infinity,
            child: FilledButton(
                onPressed: (){
                  presenter.pressLogOut(
                          (_) => exit(0),
                          (error) => showErrorDialog(context, error));
                },
                child: Text("Выход")),
          ),
        ),
      ),
    );
  }
}
