import 'package:prof2024/data/models/model_auth.dart';
import 'package:prof2024/data/models/model_platform.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../models/model_news.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signUp(
    ModelAuth modelAuth
    ) async {
  return await supabase.auth.signUp(
      email: modelAuth.email,
      password: modelAuth.password);
}

Future<AuthResponse> logIn(
    ModelAuth modelAuth
    ) async {
  return await supabase.auth.signInWithPassword(
      email: modelAuth.email,
      password: modelAuth.password);
}

Future<void> logOut() async {
  await supabase.auth.signOut();
}

Future<void> resetPassword(String email) async {
  await supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> sendOTP(String code, String email) async {
  return await supabase.auth.verifyOTP(
      token: code,
      type: OtpType.email,
      email: email);
}

Future<UserResponse> updatePassword(String newPassword) async {
  return await supabase.auth.updateUser(UserAttributes(password: newPassword));
}

Future<List<ModelPlatform>> getAllPlatforms() async {
  var response = await supabase
      .from("platforms")
      .select();
  return response.map(
          (Map<String, dynamic>e) => ModelPlatform.fromJson(e)
  ).toList();
}

Future<List<ModelNews>> getNews() async {
  var response = await supabase
      .from("news")
      .select()
      .order("published_at");
  return response.map(
          (Map<String, dynamic> e) => ModelNews.fromJson(e)).toList();
}