import 'package:intl/intl.dart';
import 'package:prof2024/data/models/model_platform.dart';

class ModelNews {
  final String id;
  final String title;
  final String text;
  final String idPlatform;
  final String channel;
  final String? media;
  final String type;
  final DateTime publishedAt;
  final double? geoLat;
  final double? geoLong;

  ModelNews(
      {
        required this.id,
        required this.title,
        required this.text,
        required this.idPlatform,
        required this.channel,
        this.media,
        required this.type,
        required this.publishedAt,
        this.geoLat,
        this.geoLong
      }
    );

  static ModelNews fromJson(Map<String, dynamic> json){
    return ModelNews(
        id: json["id"],
        title: json["title"],
        text: json["text"],
        media: json["media"],
        idPlatform: json["id_platform"],
        channel: json["channel"],
        type: json["type_media"],
        publishedAt: DateFormat("yyyy-MM-ddTHH:mm:ss")
            .parse(json["published_at"]),
      geoLat: json["geo_lat"],
      geoLong: json["geo_long"]
    );
  }

  String dateFormat(){
    return DateFormat("dd MMM, HH:mm", "ru")
        .format(publishedAt)
        .replaceAll(".", "");
  }

  ModelPlatform getModelPlatform(List<ModelPlatform> platforms){
    return platforms.where((element) => element.id == idPlatform).single;
  }

  String getTitle(){
    return (title.length > 500) ? "${title.substring(0, 500)}..." : title;
  }

  String getText(){
    return (text.length > 500) ? "${text.substring(0, 500)}..." : text;
  }
}
