class ModelPlatform{
  final String id;
  final String title;
  final String icon;
  final String channels;
  final String availableChannels;

  ModelPlatform(
      {
        required this.id,
        required this.title,
        required this.icon,
        required this.channels,
        required this.availableChannels
      }
    );

  String getIconUrl(){
    return icon;
  }
  bool isValid() {
    var userChannels = channels.split(", ");
    return availableChannels
        .split(", ")
        .toSet()
        .containsAll(userChannels);
  }

  static ModelPlatform fromJson(Map<String, dynamic> json){
    return ModelPlatform(
        id: json["id"],
        title: json["title"],
        icon: json["icon"],
        channels: json["default_channels"],
        availableChannels: json["allow_channels"]);
  }

  Map<String, String> toJson(Map<String, dynamic> json){
     var data = {
       "id": id,
       "title": title,
       "icon": icon,
       "default_channels": channels,
       "allow_channels": availableChannels
     };
     return data;
  }
}